(function () {
  'use strict';

  var tonePtn = /([aeiouvü]{1,2}(n|ng|r|\'er|N|NG|R|\'ER){0,1}[12345])/gi;
  var suffixPtn = /(n|ng|r|\'er|N|NG|R|\'ER)$/;
  var toneMap = {
    a: ['ā', 'á', 'ǎ', 'à', 'a'],
    ai: ['āi', 'ái', 'ǎi', 'ài', 'ai'],
    ao: ['āo', 'áo', 'ǎo', 'ào', 'ao'],
    e: ['ē', 'é', 'ě', 'è', 'e'],
    ei: ['ēi', 'éi', 'ěi', 'èi', 'ei'],
    i: ['ī', 'í', 'ǐ', 'ì', 'i'],
    ia: ['iā', 'iá', 'iǎ', 'ià', 'ia'],
    ie: ['iē', 'ié', 'iě', 'iè', 'ie'],
    io: ['iō', 'ió', 'iǒ', 'iò', 'io'],
    iu: ['iū', 'iú', 'iǔ', 'iù', 'iu'],
    o: ['ō', 'ó', 'ǒ', 'ò', 'o'],
    ou: ['ōu', 'óu', 'ǒu', 'òu', 'ou'],
    u: ['ū', 'ú', 'ǔ', 'ù', 'u'],
    ua: ['uā', 'uá', 'uǎ', 'uà', 'ua'],
    ue: ['uē', 'ué', 'uě', 'uè', 'ue'],
    ui: ['uī', 'uí', 'uǐ', 'uì', 'ui'],
    uo: ['uō', 'uó', 'uǒ', 'uò', 'uo'],
    v: ['ǖ', 'ǘ', 'ǚ', 'ǜ', 'ü'],
    ve: ['üē', 'üé', 'üě', 'üè', 'üe'],
    ü: ['ǖ', 'ǘ', 'ǚ', 'ǜ', 'ü'],
    üe: ['üē', 'üé', 'üě', 'üè', 'üe']
  };

  function getUpperCaseIndices(str) {
    var indices = [];
    for(var i = 0; i < str.length; i++) {
      if(str[i] === str[i].toUpperCase()) {
        indices.push(i);
      }
    }
    return indices;
  }

  function revertToUpperCase(str, indices) {
    var chars = str.split('');
    indices.map(function (idx) {
      chars[idx] = chars[idx].toUpperCase();
    });
    return chars.join('');
  }

  function parseCoda(coda) {
    var tone = parseInt(coda.slice(-1)) - 1;
    var vowel = coda.slice(0, -1);
    var suffix = vowel.match(suffixPtn);
    vowel = vowel.replace(suffixPtn, '');
    return {
      tone: tone,
      vowel: vowel.toLowerCase(),
      suffix: suffix && suffix[0],
      upperCaseIdxs: getUpperCaseIndices(vowel)
    };
  }

  function pinyinizeCoda(text, coda) {
    var parsedCoda = parseCoda(coda);
    var replacement = toneMap[parsedCoda.vowel][parsedCoda.tone];

    if (parsedCoda.suffix) {
      return text.replace(coda, revertToUpperCase(replacement + parsedCoda.suffix, parsedCoda.upperCaseIdxs));
    }

    return text.replace(coda, revertToUpperCase(replacement, parsedCoda.upperCaseIdxs));
  }

  var pinyinizer = {};

  pinyinizer.pinyinize = function (text) {
    var tones = text.match(tonePtn);

    if (!tones) {
      return text;
    }
    return tones.reduce(pinyinizeCoda, text);;
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = pinyinizer;
  } else if (window) {
    window.Pinyinizer = pinyinizer;
  } else {
    global.Pinyinizer = pinyinizer;
  }
})();
