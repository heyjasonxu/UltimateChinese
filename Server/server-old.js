var http = require("http");
var dict = require('./data/dictData.json');
var alasql = require('alasql');
var hanzi = require("hanzi");
var nictalk = require("nictalk")
// const tokenize = require('chinese-tokenizer').loadFile('./data/cedict_ts.u8')
//Initiate
hanzi.start();

var server = http.createServer(async function (request, response) {
    response.setHeader("Content-Type", 'application/json');
    response.setHeader("Access-Control-Allow-Origin", 'http://localhost:3000');
    var split = request.url.split('/')
    var attr = split[1].toLowerCase();
    var phrase = split[2];
    // console.log("attr", attr)
    // console.log("phrase", phrase)
    if (attr == 'simplified' || attr == 'traditional') {
        response.end(JSON.stringify(searchDictionary(attr, phrase)));
    } else if (attr == 'simplifiedten' || attr == 'traditionalten') {
        response.end(JSON.stringify(topTen(attr, phrase)));
    } else if (attr == "segment") {
        response.end(JSON.stringify(segment(phrase)))
    } else if (attr == "getexamples") {
        response.end(JSON.stringify(getExamples(phrase)))
    } else if (attr == "pinyin") {
        response.end(JSON.stringify(searchPinyin(phrase)))
    } else if (attr == "definition") {
        response.end(JSON.stringify(searchDefinition(phrase)))
    } else if(attr == "component") {

    } else if (attr == "audio") {
        audio(phrase)
        // response.end(JSON.stringify(audio(phrase)))
    }
})

function audio(phrase) {
    var Chinese = new nictalk();
    Chinese.setParams({ "language": "zh", "directory": "./chinese/" });
    Chinese.speak("voice", decodeURIComponent(phrase), function () { })
}

function component(phrase) {
    return hanzi.segment(decodeURIComponent(phrase))
}

function searchDictionary(type, phrase) {
    var decode = decodeURIComponent(phrase)
    // console.log(decodeURIComponent(phrase))
    var res = [];
    // res = alasql("SELECT * FROM ? WHERE Simplified=?", [dict, decode]);
    if (type.toLowerCase() == "traditional") {
        res = alasql("SELECT * FROM ? WHERE Traditional=?", [dict, decode]);
    } else if (type.toLowerCase() == "simplified") {
        res = alasql("SELECT * FROM ? WHERE Simplified=?", [dict, decode]);
    }
    return res
}

function topTen(type, phrase) {
    var decode = decodeURIComponent(phrase) + '%'
    // console.log(decodeURIComponent(phrase))
    var res = [];
    // res = alasql("SELECT * FROM ? WHERE Simplified=?", [dict, decode]);
    if (type.toLowerCase() == "traditionalten") {
        res = alasql("SELECT * FROM ? WHERE Traditional LIKE ? LIMIT 10", [dict, decode]);
    } else if (type.toLowerCase() == "simplifiedten") {
        res = alasql("SELECT * FROM ? WHERE Simplified LIKE ? LIMIT 10", [dict, decode]);
    }
    return res
}

function searchPinyin(phrase) {
    var res = [];
    res = alasql("SELECT * FROM ? WHERE PlainPinyin LIKE ? LIMIT 10", [dict, phrase]);
    return res
}

function segment(phrase) {
    return hanzi.segment(decodeURIComponent(phrase))
    // return JSON.stringify(tokenize(decodeURIComponent(phrase)), null, '  ');
}

function getExamples(phrase) {
    console.log(decodeURIComponent(phrase))
    return hanzi.getExamples(decodeURIComponent(phrase))
}

function searchDefinition(phrase) {
    var res = [];
    var wcPhrase = "%/" + phrase + "%";
    res = alasql("SELECT * FROM ? WHERE Definition LIKE ? LIMIT 10", [dict, wcPhrase]);
    // console.log("res", res)
    return res
}

server.listen(8080, function () {
    console.log("I'm listening on port 8080")
})