const express = require('express');
const hanzi = require("hanzi");
const mongo = require('mongodb');
hanzi.start();

const app = express();
const port = process.env.PORT || 5000;

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.get('/api/hello', (req, res) => {
    res.send({ express: 'Hello From Express' });
});

app.get('/api/segment/:phrase', (req, res) => {
    res.send(hanzi.segment(decodeURIComponent(req.params.phrase)));
});

app.get('/api/examples/:term', (req, res) => {
    res.send(hanzi.getExamples(decodeURIComponent(req.params.term)));
});

app.get('/api/decompose/:term', (req, res) => {
    res.send(hanzi.decompose(decodeURIComponent(req.params.term)));
});

app.get('/api/dict/character/:term', (req, res) => {
    var MongoClient = mongo.MongoClient;
    var url = "mongodb://localhost:27017";
    MongoClient.connect(url, function (err, client) {
        if (err) {
            console.log("Unable to connect to server", err)
        } else {
            console.log("Connection Established");
            var db = client.db("chinese_dict");
            var collection = db.collection("char_dict");
            // var regexp = new RegExp('/' + req.params.term.toLowerCase() + '/');
            var term = req.params.term

            collection.find({
                character: term
            }).toArray(function (err, result) {
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.send(result);
                } else {
                    res.send("[]")
                }
            })
        }
        client.close();
    })
})


app.get('/api/dict/zh/:set/:term', (req, res) => {
    var MongoClient = mongo.MongoClient;
    var url = "mongodb://localhost:27017";
    MongoClient.connect(url, function (err, client) {
        if (err) {
            console.log("Unable to connect to server", err)
        } else {
            console.log("Connection Established");
            var db = client.db("chinese_dict");
            var collection = db.collection("sorted_dict");
            var term = decodeURIComponent(req.params.term)
            var regexp = new RegExp("^" + term);
            var set = req.params.set
            if (set == "simplified") {
                collection.find({
                    Simplified: term
                }).toArray(function (err, result) {
                    if (err) {
                        res.send(err);
                    } else if (result.length) {
                        res.send(result);
                    } else {
                        res.send("[]")
                    }
                })
            } else if (set == "traditional") {
                collection.find({
                    Traditional: term
                }).toArray(function (err, result) {
                    if (err) {
                        res.send(err);
                    } else if (result.length) {
                        res.send(result);
                    } else {
                        res.send("[]")
                    }
                })
            } else if (set == "traditionallike") {
                collection.find({
                    Traditional: regexp
                }).limit(10).toArray(function (err, result) {
                    if (err) {
                        res.send(err);
                    } else if (result.length) {
                        res.send(result);
                    } else {
                        res.send("[]")
                    }
                })
            } else if (set == "simplifiedlike") {
                collection.find({
                    Simplified: regexp
                }).limit(10).toArray(function (err, result) {
                    if (err) {
                        res.send(err);
                    } else if (result.length) {
                        res.send(result);
                    } else {
                        res.send("[]")
                    }
                })
            }
        }
        client.close();
    })
})

app.get('/api/dict/en/:term', (req, res) => {
    var MongoClient = mongo.MongoClient;
    var url = "mongodb://localhost:27017";
    MongoClient.connect(url, function (err, client) {
        if (err) {
            console.log("Unable to connect to server", err)
        } else {
            console.log("Connection Established");
            var db = client.db("chinese_dict");
            var collection = db.collection("sorted_dict");
            var regexp = new RegExp('/' + req.params.term.toLowerCase() + '/');
            collection.find({
                LowDef: regexp
            }).toArray(function (err, result) {
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.send(result);
                } else {
                    res.send("[]")
                }
            })
        }
        client.close();
    })
})

app.get('/api/dict/:term', (req, res) => {
    var MongoClient = mongo.MongoClient;
    var url = "mongodb://localhost:27017";
    MongoClient.connect(url, function (err, client) {
        if (err) {
            console.log("Unable to connect to server", err)
        } else {
            console.log("Connection Established");
            var db = client.db("chinese_dict");
            var collection = db.collection("sorted_dict");
            var term = decodeURIComponent(req.params.term).toLowerCase()
            var regexp = new RegExp('/' + term + '/');
            var regexp1 = new RegExp('/to ' + term + '/')
            collection.find({
                "$or": [
                    { Simplified: term },
                    { Traditional: term },
                    { LowDef: regexp },
                    { LowDef: regexp1 },
                    { PlainPinyin: term }
                ]
            }).toArray(function (err, result) {
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.send(result);
                } else {
                    res.send("[]")
                }
            })
        }
        client.close();
    })
})

app.get('/api/sentences/:term', (req, res) => {
    var MongoClient = mongo.MongoClient;
    var url = "mongodb://localhost:27017";
    MongoClient.connect(url, function (err, client) {
        if (err) {
            console.log("Unable to connect to server", err)
        } else {
            console.log("Connection Established");
            var db = client.db("sentence");
            var collection = db.collection("ch_sent");
            var term = decodeURIComponent(req.params.term).toLowerCase()
            var regexp = new RegExp('^.*/b' + term + '/b.*$');
            var regexp1 = new RegExp('/' + term + '/');
            // var regexp1 = new RegExp('/to ' + term + '/')
            collection.find({
                "$or": [
                    { text: regexp },
                    //{ text: regexp1 }
                ]
            },
            ).limit(10).toArray(function (err, result) {
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.send(result);
                } else {
                    res.send("[]")
                }
            })
        }
        client.close();
    })
})



app.listen(port, () => console.log(`Listening on port ${port}`));
