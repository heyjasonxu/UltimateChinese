import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import RadicalCard from "../Components/RadicalCard"
import HoverChar from "../Components/hoverChar"
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import LinearProgress from '@material-ui/core/LinearProgress';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import RadSearch from "../Components/RadSearch"
var radicalList = require("../data/radicals.json")
var sortJsonArray = require('sort-json-array');

const styles = theme => ({
    root: {
        display: 'flex',

    },
    formControl: {
        margin: theme.spacing.unit * 1,
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
    },
    root: {
        flexGrow: 1,
    },
});

class ChineseRadical extends Component {

    constructor() {
        super()
        this.state = {
            title: "Traditional Radicals",
            CharSet: 'Traditional',
            radicalCards: [],
            radicalsList: [],
            loading: false,
            order: "Frequency"
        }


    }
    componentDidMount() {
        this.getList()
        this.getRadicals()
    }

    handleChange = async event => {

        await this.setState({
            CharSet: event.target.value,
            title: event.target.value + " Radicals",
        });
        this.getRadicals()
    };

    handleSortChange = async event => {
        await this.setState({ [event.target.name]: event.target.value });
        this.sort()
    };

    listExamples(input) {
        var list = []
        // var split = input.split("、")
        for (var i in input) {
            // list.push(<HoverChar char={split[i]}/>)
            list.push(input[i])
        }
        return list
    }

    listVariants(input) {
        var list = []
        if (input != null) {
            // var split = input.split(",")
            for (var i in input) {
                // list.push(<HoverChar char={split[i]}/>)
                list.push(input[i])
            }
        }
        return list
    }

    promiseA() {
        return new Promise((resolve, reject) => {
            let wait = setTimeout(() => {
                //   clearTimout(wait);
                resolve('Promise A win!');
            }, 200)
        })
    }

    async getList() {
        var radicals;
        // if (this.state.order == "Frequency") {
        //     radicals = sortJsonArray(radicalList, "Frequency No", "asc")
        // } else if (this.state.order == "Stroke Count") {
        //     radicals = sortJsonArray(radicalList, "No.", "asc")
        // }
        await this.setState({
            radicalsList: radicalList
        })
        // console.log("list", this.state.radicalsList)
    }

    async getRadicals() {
        await this.setState({
            radicalCards: [],
            loading: true
        })
        var list = []
        // var radicals = radicalList["Frequency"]
        var radicals = this.state.radicalsList
        // if(this.state.order == "Frequency") {
        //     radicals = sortJsonArray(radicalList, "Frequency No", "asc")
        // } else if(this.state.order == "Stroke Count") {
        //     radicals = sortJsonArray(radicalList, "No.", "asc")
        // }

        // console.log("radicals", radicals)
        if (this.state.CharSet == "Traditional") {
            for (var i in radicals) {
                var exampleList = this.listExamples(radicals[i]["Examples"])
                var variantList = this.listVariants(radicals[i]["Variants"])
                // console.log("sample", exampleList.slice(0, 3))
                var count = parseInt(i) + 1
                list.push(<div style={{ "display": "inline-block" }}><RadicalCard count={count} id={i} key={i} radical={radicals[i]["Radical (Traditional)"]}
                    definition={radicals[i]["Meaning"]} pinyin={radicals[i]["Pīnyīn"]}
                    examples={<HoverChar char={exampleList} sample={exampleList.slice(0, 3)} columns={2} icon={true} />}
                    variants={<HoverChar char={variantList} sample={variantList.slice(0, 3)} columns={1} />}
                /></div>)
            }
        } else if (this.state.CharSet == "Simplified") {
            for (var i in radicals) {
                var exampleList = radicals[i]["Examples (Simplified)"] ?
                    radicals[i]["Examples (Simplified)"] : radicals[i]["Examples"]
                var variantList = radicals[i]["Variant (Simplified)"]
                    ? radicals[i]["Variant (Simplified)"] : "" + (radicals[i]["Variants"] ? radicals[i]["Variants"] : "")
                // console.log("Variant", variantList)
                var count = parseInt(i) + 1
                list.push(<div style={{ "display": "inline-block" }}><RadicalCard count={count} id={i} key={i} radical={radicals[i]["Radical (Simplified)"]}
                    definition={radicals[i]["Meaning"]} pinyin={radicals[i]["Pīnyīn"]}
                    examples={<HoverChar char={exampleList} sample={exampleList.slice(0, 3)} columns={2} icon={true} />}
                    variants={<HoverChar char={variantList} sample={variantList.slice(0, 3)} columns={1} />}
                /></div>)
            }
        }

        // await this.promiseA()

        await this.setState({
            loading: false,
            radicalCards: list
        })
        // console.log("radicals", this.state.radicals)
        // return list;
    }

    async sort() {
        var list = sortJsonArray(this.state.radicalsList,
            this.state.order == "Frequency" ? "Frequency No" : "No.", "asc")
        // if(this.state.order == "Frequency") {
        //     list = sortJsonArray(this.state.radicalsList, "Frequency No", "asc")
        // } else if(this.state.order == "Stroke Count") {
        //     list = sortJsonArray(this.state.radicalsList, "No.", "asc")
        // }

        // console.log("list", list)   
        await this.setState({
            radicalsList: list
        })
        this.getRadicals()
    }

    async search(term) {
        var list = radicalList;
        list = sortJsonArray(list,
            this.state.order == "Frequency" ? "Frequency No" : "No.", "asc")
        var newList = []
        if (term.length > 0) {
            for (var i in list) {
                if (list[i]["Meaning"].includes(term) || list[i]["Radical (Traditional)"].includes(term) ||
                    list[i]["Radical (Simplified)"].includes(term) || list[i]["PlainPinyin"].includes(term) ||
                    (list[i]["Stroke count"] + "").includes(term)){
                        newList.push(list[i])
                    }
                    
            }
        } else if (term == "") {
            for (var i in list) {
                newList.push(list[i])
            }
        }
        await this.setState({
            radicalsList: newList
        })
        this.getRadicals()
    }


    render() {
        const { classes } = this.props;

        return (
            <div>
                <div className="d-flex justify-content-between">
                    <div></div>
                    <Typography variant="display3" className="">
                        <div className="text-center" style={{ "fontFamily": "Comic Sans MS" }}>{this.state.title}</div>
                    </Typography>
                    <RadSearch search={this.search.bind(this)} />
                    <div>
                        <FormControl component="fieldset" className={classes.formControl}>
                            {/* <FormLabel component="legend"></FormLabel> */}
                            <RadioGroup
                                aria-label="CharSet"
                                name="CharSet"
                                className={classes.group}
                                value={this.state.CharSet}
                                onChange={this.handleChange}
                            >
                                <FormControlLabel value="Simplified" control={<Radio />} label="Simplified" />
                                <FormControlLabel value="Traditional" control={<Radio />} label="Traditional" />
                            </RadioGroup>
                        </FormControl>
                    </div>
                    <div className="">

                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="age-helper">Sort</InputLabel>
                            <Select
                                value={this.state.order}
                                onChange={this.handleSortChange}
                                input={<Input name="order" id="order" />}
                            >
                                <MenuItem value="Frequency">
                                    <em>Frequency</em>
                                </MenuItem>
                                <MenuItem value={"Stroke Count"}>Stroke Count</MenuItem>
                            </Select>
                            {/* <FormHelperText>Some important helper text</FormHelperText> */}
                        </FormControl>
                    </div>



                </div>
                {this.state.loading
                    ?
                    <div className={classes.root}>
                        <LinearProgress />
                        <br />
                        {/* <LinearProgress color="secondary" /> */}
                    </div>
                    :
                    <div className="d-flex flex-wrap">
                        {this.state.radicalCards}
                    </div>

                }
                {/* <RadicalResults radicals={ this.getRadicals() } /> */}
            </div>
        );
    }
}

ChineseRadical.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ChineseRadical);
