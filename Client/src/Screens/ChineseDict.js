import React, { Component } from 'react';
import '../App.css';
import SearchBar from "../Components/SearchBar";
import axios from 'axios'
import pinyinizer2 from '../pinyinizer-2'
import isChinese from "is-chinese"
import opencc from 'node-opencc';
import Row from "../Components/Row"
import Drawer from "../Components/Drawer"
import { connect } from "react-redux"
const instance = axios.create({ baseURL: 'http://jasonxu.me:5000' })
const instance2 = axios.create({ baseURL: 'https://www.google.com/inputtools/request?ime=pinyin&ie=utf-8&oe=utf-8&app=translate&num=10&text=' })


class ChineseDict extends Component {

  constructor() {
    super();
    this.state = {
      text: "",
      region: 'china',
      chars: [],
      rows: [],
      termType: "",
      font: "KaiTi"
    }
  }

  async getRows() {
    await this.setState({
      rows: []
    })
    var rows = [];
    var def = this.state.chars
    // console.log("def", def)
    // def = this.orderRows(def)
    for (var j in def) {
      // console.log('def', def)
      for (var i in def[j]) {
        var examples = []
        // var examples = await this.serverTool("examples", def[j][i][this.state.charSet])
        // console.log("examples", examples)
        var definition = pinyinizer2.pinyinize(def[j][i].Definition)
        // console.log("def", definition)
        var data = {
          characters: def[j][i][this.props.charSet.main],
          charSet: this.props.charSet.main,
          altSet: this.props.charSet.alt,
          altChar: def[j][i][this.props.charSet.alt],
          pinyin: def[j][i].PrettyPinyin,
          numPinyin: def[j][i].NumPinyin,
          definition: definition.substring(1, definition.length),
          examples: examples,
          font: this.state.font
        }
        rows.push(<Row id={j + " " + i} key={j + " " + i} data={data} />)
        // console.log("row-id-key", j + " " + i)
      }
    }
    await this.setState({
      rows: rows
    })
    // console.log('rows', this.state.rows)
  }

  orderRows(rows) {
    var high = []
    var low = []
    var test = rows
    for (var i in test) {
      var sep = test[i].LowDef
      // console.log("sep", sep)
    }
    return test
  }

  async handleSubmit() {
    if (this.state.text !== '') {
      await this.setState({ chars: "" })
      // var searchType = this.state.charSet.toLowerCase();
      var word = opencc.traditionalToSimplified(this.state.text)
      console.log("word", word)
      var result = await this.serverDict(word) //check if in dictionary

      var sep = []
      if (result.length > 0) {
        sep.push(result)
      }

      if (sep.length !== 0) { //term is in dictionary
        var char = [];
        char.push(word)
        this.setState({
          termType: "complete",
          char: char
        })
      }
      if (sep.length == 0) { // check if term is chinese
        if (isChinese(word)) {
          var segment = await this.serverTool('segment', word)
          // console.log("segment", segment)
          for (var i in segment) {
            var word = segment[i]
            // console.log("word", word)
            var def = await this.serverDict(word)
            // console.log("test", def)
            if (def.length > 0) {
              sep.push(def)
            }
          }
        }
        await this.setState({
          termType: "chinese"
        })
      }
      this.orderRows(sep)

      await this.setState({
        chars: sep
      })
      await this.getRows()
    }
  }

  serverTool(attr, phrase) {
    return new Promise(function (resolve, reject) {
      // Do async job
      instance.get('/api/' + attr + '/' + phrase)
        .then(response => {
          resolve(response.data)
        }).catch(err => reject(err))
    })
  }

  serverDict(term) {
    return new Promise(function (resolve, reject) {
      // Do async job
      instance.get('/api/dict/' + term)
        .then(response => {
          resolve(response.data)
        }).catch(err => reject(err))
    })
  }


  googleInput(pinyin) {
    return new Promise(function (resolve, reject) {
      // Do async job
      instance2.get(pinyin)
        .then(response => {
          // console.log(response)
          resolve(response.data[1][0][1])
        }).catch(err => reject(err))
    })
  }

  async test() {

  };

  //switch between character sets
  switchSet = async function () {
    this.handleSubmit()
  }

  switchFont = async function (font) {
    await this.setState({ font: font })
    this.getRows()
  }

  //text is changed
  onChange = async function (value) {
    this.setState({
      text: value.target.value,
    })
    // console.log(this.state.text)
  }


  // handleRadio = async function (event) {
  //   await this.setState({ charSet: event.target.value })
  //   if (this.state.charSet == "Simplified") {
  //     this.setState({ altSet: "Traditional" })
  //   } else {
  //     this.setState({ altSet: "Simplified" })
  //   }
  //   this.handleSubmit()
  // }.bind(this);

  // handleChange = name => event => {
  //   this.setState({
  //     [name]: event.target.value,
  //   });
  // };

  handleChange = text => {
    // console.log(text)
    this.setState({
      text: text
    })
  };

  render() {
    return (
      <div className="App mx-auto">

        <div className="d-flex justify-content-center">
          <SearchBar search={this.handleSubmit.bind(this)} onChange={this.handleChange.bind(this)} switchSet={this.switchSet.bind(this)} />
          <Drawer switchSet={this.switchSet.bind(this)} switchFont={this.switchFont.bind(this)} />
        </div>

        {/* <Button variant="contained" color="primary" size="medium" onClick={this.test.bind(this)}>
          Test
                </Button> */}
        <div className="d-flex justify-content-center">
          <div className="List d-flex flex-column">{this.state.rows}</div>
        </div>
        {/* <Results result={this.state.rows} /> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  charSet: state.set.charSet,
})

export default connect(mapStateToProps)(ChineseDict);
