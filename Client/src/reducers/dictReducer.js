import { SWITCH_SET, SWITCH_FONT } from "../actions/types";

const initialState = {
    charSet: {
        main: "Simplified",
        alt: "Traditional"
    },
    font: "KaiTi"
};

export default function switchSet(state = initialState, { type, payload }) {
    switch (type) {
        case SWITCH_SET:
            return {
                charSet: payload.charSet,
                font: state.font
            };
        case  SWITCH_FONT:
            return {
                charSet: state.charSet,
                font: payload.font
            };
        default:
            return state;
    }
}
