import axios from 'axios'

class Hanzi {
    constructor(hanzi, url) {
        this.hanzi = hanzi
        this.instance = axios.create({ baseURL: url })
    }

    queryServer(attr, phrase) {
        return new Promise(function (resolve, reject) {
          // Do async job
          instance.get('/' + attr + '/' + phrase)
            .then(response => {
              resolve(response.data)
            }).catch(err => reject(err))
        })
      }
}