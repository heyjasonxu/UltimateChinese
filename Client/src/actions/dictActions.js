import { SWITCH_SET, SWITCH_FONT } from "./types";

export const switchSet = (set) => {
    let newSet = {
        main: "Simplified",
        alt: "Traditional"
    }
    if (set == "Simplified") {
        newSet = {
            main: "Traditional",
            alt: "Simplified"
        }
    }
    return {
        type: SWITCH_SET,
        payload: {
            charSet: newSet
        }
    }
}

export const switchFont = (font) => {
    return {
        type: SWITCH_FONT,
        payload: {
            font: font
        }
    }
}

// const switchSet = {
//     type: SWITCH_SET,
//     payload: {
//         set: "test"
//     }
// }


