import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Settings from '@material-ui/icons/Settings';
import { connect } from 'react-redux';
import { switchSet, switchFont } from '../actions/dictActions'

import FormControlLabel from '@material-ui/core/FormControlLabel';
import { compose } from 'redux';

const styles = {
  list: {
    width: 250,
    padding: "25px"
  },
  fullList: {
    width: 'auto',
  },
  group: {

  },
  formControl: {

  }
};

class TemporaryDrawer extends React.Component {

  constructor() {
    super();
    this.state = {
      top: false,
      left: false,
      bottom: false,
      right: false,
      checkedA: true,
      value: 'Simplified',
      font: "KaiTi"
    };
  }

  componentWillMount() {

  }

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  // async switch() {
  //   this.props.onSwitchSet(this.props.set.set)
  //   if (this.state.charSet == "Simplified") {
  //     await this.setState({
  //       charSet: "Traditional",
  //       checkedA: !this.state.checkedA,
  //     })
  //   } else {
  //     await this.setState({
  //       charSet: "Simplified",
  //       checkedA: !this.state.checkedA,
  //     })
  //   }
  //   this.props.switchSet(this.props.set.set);
  //   console.log(this.props.set)
  // }

  setFont = event => {
    this.setState({ [event.target.name]: event.target.value });
    this.props.onSwitchFont(event.target.value)
    this.props.switchFont()
  };

  setCharSet(event) {
    this.setState({ value: event.target.value });
    this.props.onSwitchSet(this.props.charSet.main)
    this.props.switchSet();
  };

  render() {
    const { classes } = this.props;

    const sideList = (
      <div className={classes.list}>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">Character Set</FormLabel>
          <RadioGroup
            aria-label="Character Set"
            name=""

            value={this.state.value}
            onChange={this.setCharSet.bind(this)}
          >
            <FormControlLabel value="Simplified" control={<Radio />} label="Simplified (简体字)" />
            <FormControlLabel value="Traditional" control={<Radio />} label="Traditional (繁體字)" />
          </RadioGroup>
        </FormControl>
        <Divider />
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="age-helper">Font</InputLabel>
          <Select
            value={this.state.font}
            onChange={this.setFont.bind(this)}
            input={<Input name="font" id="font" />}
          >
            <MenuItem value="KaiTi">
              <em>KaiTi</em>
            </MenuItem>
            <MenuItem value="Microsoft YaHei">Microsoft YaHei</MenuItem>
            <MenuItem value={"FangSong"}>FangSong</MenuItem>
            <MenuItem value={"STKaiti"}>STKaiti</MenuItem>
          </Select>
          {/* <FormHelperText>Some important helper text</FormHelperText> */}
        </FormControl>

        <Divider />
      </div>
    );

    // const fullList = (
    //   <div className={classes.fullList}>
    //     <List>{mailFolderListItems}</List>
    //     <Divider />
    //     <List>{otherMailFolderListItems}</List>
    //   </div>
    // );

    return (
      <div className="align-middle">
        <Button onClick={this.toggleDrawer('left', true)}>
          <Settings onClick={this.toggleDrawer('left', true)} />
        </Button>
        <Drawer anchor="left" open={this.state.left} onClose={this.toggleDrawer('left', false)}>
          <div
            tabIndex={0}
            role="button"
          //onClick={this.toggleDrawer('left', false)}
          //onKeyDown={this.toggleDrawer('left', false)}
          >
            {sideList}
          </div>
        </Drawer>
        {/* <Drawer anchor="top" open={this.state.top} onClose={this.toggleDrawer('top', false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('top', false)}
            onKeyDown={this.toggleDrawer('top', false)}
          >
            {fullList}
          </div>
        </Drawer>
        <Drawer
          anchor="bottom"
          open={this.state.bottom}
          onClose={this.toggleDrawer('bottom', false)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('bottom', false)}
            onKeyDown={this.toggleDrawer('bottom', false)}
          >
            {fullList}
          </div>
        </Drawer>
        <Drawer anchor="right" open={this.state.right} onClose={this.toggleDrawer('right', false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('right', false)}
            onKeyDown={this.toggleDrawer('right', false)}
          >
            {sideList}
          </div>
        </Drawer> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  charSet: state.set.charSet,
  font: state.set.font
})

const mapActionsToProps = {
  onSwitchSet: switchSet,
  onSwitchFont: switchFont
}

TemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapActionsToProps)
)(TemporaryDrawer);