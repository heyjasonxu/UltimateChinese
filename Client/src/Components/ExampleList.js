// import React from 'react';
// import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/core/styles';
// import List from '@material-ui/core/List';
// import ListItem from '@material-ui/core/ListItem';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItemText from '@material-ui/core/ListItemText';
// import Divider from '@material-ui/core/Divider';
// import InboxIcon from '@material-ui/icons/Inbox';
// import DraftsIcon from '@material-ui/icons/Drafts';

// const styles = theme => ({
//     root: {
//         width: '50%',
//         maxWidth: 360,
//         backgroundColor: theme.palette.background.paper,
//     },
// });



// class ExampleList extends React.Component {
//     handleClick(example) {
//         console.log("1")
//         this.props.handleClick(example)
//     }

//     createList() {
//         console.log(this.props.examples)
//         var list = []
//         var count = 0
//         var examples = this.props.examples
//         for (var i in examples) {
//             for (var j in examples[i]) {
//                 // console.log(this.props.numPinyin)
//                 var pinyin = examples[i][j]["pinyin"].replace("u:", "v").toLowerCase()
//                 var example = examples[i][j][this.props.charSet.toLowerCase()]
//                 if ((example !== this.props.characters) && pinyin.includes(this.props.numPinyin.toLowerCase())) { //no repeat in examples
//                     // console.log("ttt")
//                     count++
//                     // list += count + examples[i][j][this.props.charSet] + " "
//                     var example = examples[i][j][this.props.charSet.toLowerCase()]
//                     // list.push(<span onClick={this.handleClick.bind(this, example)}>{example}  </span>)
//                     list.push(
//                         <ListItem button >
//                             <ListItemText primary={example} onClick={this.handleClick.bind(this, example)} />
//                         </ListItem >
//                     )
//                     // console.log(count)
//                     if (count == 10) {
//                         break;
//                     }
//                 }
//             }
//             if (count == 10) {
//                 break;
//             }
//         }
//         console.log("list", list)
//         return list
//     }

//     render() {
//         const { classes } = this.props;
//         return (
//             <div>
//                 <div className={classes.root}>
//                     <List component="nav">
//                         {this.createList()}
//                     </List>
//                 </div>
//                 <div className={classes.root}>
//                     <List component="nav">
//                         {this.createList()}
//                     </List>
//                 </div>
//             </div>
//         );
//     }


// }

// ExampleList.propTypes = {
//     classes: PropTypes.object.isRequired,
// };

// export default withStyles(styles)(ExampleList);


import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 500,
        height: 'auto',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
});

/**
 * The example data is structured as follows:
 *
 * import image from 'path/to/image.jpg';
 * [etc...]
 *
 * const tileData = [
 *   {
 *     img: image,
 *     title: 'Image',
 *     author: 'author',
 *   },
 *   {
 *     [etc...]
 *   },
 * ];
 */
class TitlebarGridList extends React.Component {
    constructor() {
        super()
        this.state = {
            list: [],
        }
    }
    componentDidMount() {
        this.createList()
    }

    handleClick(example) {
        console.log("1")
        this.props.handleClick(example)
    }

    async createList() {
        // console.log(this.props.data.examples)
        var list = []
        var count = 0
        var examples = this.props.examples
        console.log("examples", examples)
        for (var i in examples) {
            for (var j in examples[i]) {
                // console.log(this.props.numPinyin)
                var pinyin = examples[i][j]["pinyin"].replace("u:", "v").toLowerCase()
                // var example = examples[i][j][this.props.charSet.toLowerCase()]
                if ((example !== this.props.data.characters) && pinyin.includes(this.props.data.numPinyin.toLowerCase())) { //no repeat in examples
                    // console.log("ttt")
                    count++
                    // list += count + examples[i][j][this.props.charSet] + " "
                    var example = examples[i][j][this.props.data.charSet.toLowerCase()]
                    // list.push(<span onClick={this.handleClick.bind(this, example)}>{example}  </span>)
                    // list.push(
                    //     <ListItem button >
                    //         <ListItemText primary={example} onClick={this.handleClick.bind(this, example)} />
                    //     </ListItem >
                    // )
                    list.push(example)
                    // console.log(count)
                    if (count == 10) {
                        break;
                    }
                }
            }
            if (count == 10) {
                break;
            }
        }
        console.log("list", list)
        this.setState({
            list: list
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <GridList cellHeight={50} className={classes.gridList}>
                    <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                        <ListSubheader component="div">Examples</ListSubheader>
                    </GridListTile>
                    {this.state.list.map(item => (
                        <ListItem button >
                            <ListItemText style={{width: "auto"}} primary={item} onClick={this.handleClick.bind(this, item)} />
                        </ListItem >
                    ))}
                </GridList>
            </div>
        );
    }
}


TitlebarGridList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TitlebarGridList);