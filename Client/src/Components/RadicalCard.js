import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import axios from 'axios'

const instance = axios.create({ baseURL: 'http://jasonxu.me:5000' })
// import Details from "./switch";


const styles = theme => ({
    card: {
        width: "140px",
        height: "100px"
        // minWidth: "350px",
        // margin: "2px",
        // display: "inline"
    },
    media: {
        height: 0,
        paddingTop: '35%', // 16:9
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    Primary: {
        fontSize: 16,
    },
    button: {
        position: 'relative',
        left: 0,
        top: 0,
        width: 25,
        height: 25
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
    content: {
        padding: 5,
    },
    margin: {
        margin: theme.spacing.unit * 1,
    },
    padding: {
        padding: `0 ${theme.spacing.unit * 2}px`,
    },
    badge: {
        top: 1,
        left: 10,
        // The border color match the background color.
      },
});


class RadicalCard extends React.Component {
    constructor() {
        super()
        this.state = {
            expanded: false,
            charSet: "simplified",
            viewExampleChar: false

        };
        // this.handleExampleClick = this.handleExampleClick.bind(this)
    }

    render() {
        const { classes } = this.props;
        var style = {
            pinyin: {
                fontSize: 13
            },
            radical: {
                fontFamily: "KaiTi",
                fontWeight: "bold",
                fontSize: 50,
                margin: "4px"
            },
            main: {
                width: "80px"
            },
            examples: {
                fontFamily: "KaiTi",
                fontSize: 14,
                width: "16px"
                // fontWeight: "bold"
            },
            variants: {
                fontFamily: "KaiTi",
                fontSize: 14,
                width: "16px",
                marginTop: 20
                // fontWeight: "bold"
            },
        }

        return (
            <div>
                <Badge className={classes.margin} badgeContent={this.props.count} color="secondary" classes={{ badge: classes.badge }}>
                <div className={classes.card}>
                    <div className={classes.content}>
                        <div className="d-flex justify-content-center">
                            <div style={style.variants} className="d-flex  flex-column">
                                {this.props.variants}
                            </div>
                            <div style={style.main} className="">
                                <Typography variant="headline">
                                    <div style={{ "textAlign": "center" }}>
                                        <div style={style.radical}>{this.props.radical}</div>

                                    </div>
                                </Typography>
                                <Typography>
                                    <div style={{ "textAlign": "center" }}>
                                        <div style={style.pinyin}>{this.props.pinyin}</div>
                                        {this.props.definition}
                                    </div>
                                </Typography>
                            </div>
                            <div style={style.examples} className="d-flex flex-column">
                                {this.props.examples}
                            </div>

                        </div>

                    </div>
                    {/* <CardActions className={classes.actions} disableActionSpacing>
                        <IconButton aria-label="Add to favorites">
                            <FavoriteIcon />
                        </IconButton>
                        <IconButton aria-label="Share">
                            <ShareIcon />
                        </IconButton>
                        <IconButton
                            className={classnames(classes.expand, {
                                [classes.expandOpen]: this.state.expanded,
                            })}
                            onClick={this.handleExpandClick}
                            aria-expanded={this.state.expanded}
                            aria-label="Show more"
                        >
                            <ExpandMoreIcon />
                        </IconButton>
                    </CardActions> */}
                </div>
                </Badge>
            </div>
            
        );
    }
}

RadicalCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RadicalCard);