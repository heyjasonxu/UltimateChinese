import React from 'react';
import PropTypes from 'prop-types';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import More from '@material-ui/icons/MoreVert';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    popover: {
        pointerEvents: 'none',
    },
    paper: {
        padding: theme.spacing.unit,
    },
});

class MouseOverPopover extends React.Component {
    state = {
        anchorEl: null,
    };

    componentWillMount() {
        this.setState({
            width: this.props.columns * 45 + "px"
        })
    }

    handlePopoverOpen = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handlePopoverClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        const { classes } = this.props;
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <div>
                <Typography
                    aria-owns={open ? 'mouse-over-popover' : null}
                    aria-haspopup="true"
                    onMouseEnter={this.handlePopoverOpen}
                    onMouseLeave={this.handlePopoverClose}
                >
                    <span style={{ "fontFamily": "KaiTi, STKaiti, 华文楷体, serif" }}>{this.props.sample}</span>
                    {this.props.sample.length != 0 && this.props.icon ?
                        <More style={{"height": 12, "width": 14}}/>
                        :
                        ""
                    }
                </Typography>
                <Popover
                    id="mouse-over-popover"
                    className={classes.popover}
                    classes={{
                        paper: classes.paper,
                    }}
                    open={open}
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    onClose={this.handlePopoverClose}
                    disableRestoreFocus
                >
                    <Typography style={{ "width": this.state.width }} variant="display2">
                        <span style={{ "fontFamily": "KaiTi, STKaiti, 华文楷体, serif" }}>{this.props.char}</span>

                    </Typography>
                </Popover>
            </div>
        );
    }
}

MouseOverPopover.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MouseOverPopover);