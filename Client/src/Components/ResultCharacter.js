import React from 'react';
import HoverClick from "./HoverClick"
import { connect } from "react-redux"

class Character extends React.Component {
    constructor() {
        super();
        // this.characterClick = this.characterClick.bind(this);
    }

    checkSimpTrad(Primary, Secondary) {
        if (Primary === Secondary) {
            return ""
        }
        var alt = "("
        for (var i in Primary) {
            if (Primary[i] === Secondary[i]) {
                alt += " - "
            } else {
                alt += Secondary[i];
            }
        }
        return alt + ")"
    }

    characterClick(char, pinyin) {
        this.props.handleClick(char, pinyin)
    }

    clickCharacter() {
        var newString = []
        if (this.props.clickable) {
            var splitPinyin = this.props.data.pinyin.split(" ")
            for (var i in this.props.data.characters) {
                newString.push(
                    <HoverClick word={this.props.data.characters[i]} exampleClick={this.characterClick.bind(this,this.props.data.characters[i], splitPinyin[i])}/>
                )
            }
            return newString;
        } else {
            return this.props.data.characters
        }
    }

    async test() {

        if (this.state.font === "Microsoft YaHei") {
            await this.setState({
                font: "Simhei"
            })
        } else {
            await this.setState({
                font: "Microsoft YaHei"
            })
        }
        // console.log(this.state.font)
    }

    render() {
        const styles = {
            altChar: {
                paddingTop: "32px",
                paddingLeft: "5px",
                display: "inline-block",
                fontFamily: this.props.font
            },
            pinyin: {
                fontSize: 20
            },
            char: {
                fontSize: 34,
                fontFamily: this.props.font,
                fontWeight: "bold"
            }
        }
        return (
            <div class="d-flex justify-content-center flex-wrap">
                <div style={{ "display": "inline-block" }}>
                    <div style={styles.pinyin}>
                        {this.props.data.pinyin}
                    </div>
                    {/* <div style={[styles.char, {fontFamily: this.state.font}]}> */}
                    <div style={styles.char}>

                        {this.clickCharacter()}
                    </div>

                </div>
                {/* <span style={[styles.altChar, { fontFamily: this.state.font }]}> */}
                <div style={styles.altChar}>

                    {this.checkSimpTrad(this.props.data.characters, this.props.data.altChar)}
                </div>
                {/* <Button variant="contained" color="primary" size="medium" onClick={this.test.bind(this)}>
                    Search
                </Button> */}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    font: state.set.font
  })
  
export default connect(mapStateToProps)(Character);


