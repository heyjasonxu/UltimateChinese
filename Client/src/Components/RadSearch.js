import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class SearchBar extends Component {

    constructor() {
        super()
        this.state = {
            character: "",
            text: "",
            checkedA: true,
            charSet: "Simplified",
            font: "Microsoft YaHei"
        }
    }

    onChange(value) {
        this.setState({
            text: value.target.value,
        })
    }

  
    handleText = name => event => {
        this.setState({
            [name]: event.target.value,
        })
        // this.props.onChange(event.target.value)
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    search() {
        this.props.search(this.state.text)
    }

    render() {
        const { classes } = this.props;
        return (
            <div className="SearchBar">
                {/* <label>
                    <input id="input" type="text" value={this.state.text} onChange={this.handleText("text")} />
                </label> */}
                <TextField
                    id="search"
                    label="Search"
                    type="search"
                    value={this.state.text}
                    onChange={this.handleText('text')}
                    // className={classes.textField}
                    margin="normal"
                    helperText="Chinese, Pinyin, English or Stroke Count"
                />
                <Button variant="contained" color="primary" size="medium" onClick={this.search.bind(this)}>
                    Search
                </Button>
            </div>
        );
    }
}

SearchBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchBar);
