import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    formControl: {
      margin: theme.spacing.unit,
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing.unit * 2,
    },
  });

class SearchBar extends Component {

    constructor() {
        super()
        this.state = {
            character: "",
            text: "",
            checkedA: true,
            charSet: "Simplified",
            font: "Microsoft YaHei"
        }
    }

    onChange(value) {
        this.setState({
            text: value.target.value,
        })
    }

    async switch() {

        if (this.state.charSet == "Simplified") {
            await this.setState({
                charSet: "Traditional",
                checkedA: !this.state.checkedA,
            })
        } else {
            await this.setState({
                charSet: "Simplified",
                checkedA: !this.state.checkedA,
            })
        }
        this.props.switchSet(this.state.charSet);
    }

    handleText = name => event => {
        this.setState({
            [name]: event.target.value,
        })
        this.props.onChange(event.target.value)
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
      };

    render() {
        const { classes } = this.props;
        return (
            <div className="SearchBar">
                {/* <label>
                    <input id="input" type="text" value={this.state.text} onChange={this.handleText("text")} />
                </label> */}
                <TextField
                    id="search"
                    label="Search"
                    type="search"
                    value={this.state.text}
                    onChange={this.handleText('text')}
                    // className={classes.textField}
                    margin="normal"
                    helperText="Chinese, Pinyin or English"
                />
                <Button variant="contained" color="primary" size="medium" onClick={this.props.search}>
                    Search
                </Button>
                {/* <FormControlLabel
                    style={{ width: "200" }}
                    control={
                        <Switch
                            checked={this.state.checkedA}
                            onChange={this.switch.bind(this)}
                            value={this.state.charSet}
                            style={{ width: "200" }}
                        />
                    }
                    label={this.state.charSet}
                /> */}
                {/* <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="age-helper">Font</InputLabel>
                    <Select
                        value={this.state.font}
                        onChange={this.handleChange}
                        input={<Input name="font" id="font" />}
                    >
                        <MenuItem value="Microsoft YaHei">
                            <em>Microsoft YaHei</em>
                        </MenuItem>
                        <MenuItem value={"FangSong"}>FangSong</MenuItem>
                        <MenuItem value={"STKaiti"}>STKaiti</MenuItem>
                    </Select>
                    <FormHelperText>Some important helper text</FormHelperText>
                </FormControl> */}
            </div>
        );
    }
}

SearchBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(SearchBar);
