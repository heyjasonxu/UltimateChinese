import React, { Component } from 'react';
import AnimeCharacter from "./AnimeCharacter";
import Expansion from "./ExpansionPanel"

export default class Details extends Component {

    constructor() {
        super()
        this.state = {

            // charSet: this.props.charSet
            // charSet: "Simplified"
        }
    }
    handleClick() {
        this.props.searchExample()
    }


    getExamples() {
        // console.log("Character", this.props.characters)
        // if (this.props.characters.length == 1) {
        // var char = opencc.traditionalToSimplified(this.props.characters)
        // console.log(hanzi.getExamples(this.props.characters));
        var examples = this.props.examples
        console.log("examples", examples)
        var list = [];
        var count = 0;
        for (var i in examples) {
            for (var j in examples[i]) {
                var pinyin = examples[i][j]["pinyin"].replace("u:", "v").toLowerCase()
                var example = examples[i][j][this.props.data.charSet.toLowerCase()]
                if ((example !== this.props.data.characters) && pinyin.includes(this.props.data.numPinyin.toLowerCase())) { //no repeat in examples
                    count++
                    // list += count + examples[i][j][this.props.charSet] + " "
                    var example = examples[i][j][this.props.data.charSet.toLowerCase()]
                    list.push(<span onClick={this.handleClick.bind(this, example)}>{example}  </span>)
                    if (count == 10) {
                        break;
                    }
                }
            }
            if (count == 10) {
                break;
            }
        }
        return list
    }

    getDecomp() {
        console.log("decomp", this.props.decomp)
    }

    async sepCharacter() {
        var list = [];
        var word = this.props.data.characters
        for (var i in word) {
            var char = word[i]
            list.push(<AnimeCharacter character={char} altChar={this.props.data.altChar[i]} id={this.props.id + "" + i} key={this.key + "" + i} />)
            // console.log("character-id", this.props.id + "" + i)
            // console.log("character-key", this.key + "" + i)
        }
        await this.setState({
            stroke: list
        })
    }

    decomp1() {
        if (this.props.decomp != null) {
            var comp = []
            var decomp = this.props.decomp
            for (var i in decomp.components1) {
                comp.push(decomp.components1[i])
            }
            return comp;
        }
    }

    decomp2() {
        if (this.props.decomp != null) {
            var comp = []
            var decomp = this.props.decomp
            for (var i in decomp.components2) {
                comp.push(decomp.components2[i])
            }
            return comp
        }
    }

    handleClickExample() {
        this.props.searchExample
    }

    render() {
        var style = {
            box: {
                height: "20px",
                width: "100%",
                borderColor: "blue",
                borderWidth: "2px"
            }
        }
        return (
            <div>
                <div className="d-flex justify-content-center flex-column">
                    <Expansion id={this.props.id} key={this.props.key} stroke={this.state.stroke} decomp={this.props.decomp} charData={this.props.charData}
                        examples={this.props.examples} exampleClick={this.props.exampleClick} data={this.props.data} />
                </div>

            </div>
        );
    }
}