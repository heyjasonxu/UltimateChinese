import React from 'react';

export default class HoverClick extends React.Component {
    constructor() {
        super()
        this.state = {
            hover: false
        }
    }

    mouseEnter() {
        this.setState({
            hover: true
        })
    }

    mouseLeave() {
        this.setState({
            hover: false
        })
    }

    exampleClick() {
        this.props.exampleClick()
        // this.props.exampleClick(word, pinyin)
    }

    render() {
        var style = {
            hover: {
                // fontWeight: "bold",
                // fontSize: 16,
                color: "blue",
                textDecoration: "underline",

            },
            notHover: {
                // fontWeight: "bold",
                // color: "DarkBlue",
                // fontSize: 16
            }
        }
        return (
            <span style={this.state.hover ? style.hover : style.notHover} onMouseEnter={this.mouseEnter.bind(this)}
                onMouseLeave={this.mouseLeave.bind(this)} onClick={this.exampleClick.bind(this)}>{this.props.word}</span>
        )
    }
}