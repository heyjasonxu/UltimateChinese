import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Details from "./Details"
import ResultChar from "./ResultCharacter"
import ClearIcon from '@material-ui/icons/Clear';
import Divider from '@material-ui/core/Divider';
import axios from 'axios'
import CircularProgress from '@material-ui/core/CircularProgress';
const instance = axios.create({ baseURL: 'http://jasonxu.me:5000' })


const styles = theme => ({
    card: {
        width: "600px",
        // maxWidth: "600px",
        // minWidth: "350px",
        margin: "5px",
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    media: {
        height: 0,
        paddingTop: '20%', // 16:9
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    Primary: {
        fontSize: 16,
    },
    button: {
        position: 'relative',
        left: 0,
        top: 0,
        width: 25,
        height: 25
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
});


class ResultCard extends React.Component {
    constructor() {
        super()
        this.state = {
            expanded: false,
            charSet: "simplified",
            viewExampleChar: false

        };
    }

    componentDidMount(evt) {
        this.setState({
            charSet: this.props.charSet
        })
    }

    handleExpandClick = async (evt) => {
        this.setState(state => ({ expanded: !state.expanded }));
        var char = this.props.data.characters
        // console.log(char)
        var examples = await this.serverTool("examples", char)

        var decomp = [];
        var charData;
        if (char.length == 1) {
            decomp.push(await this.serverTool("decompose", char))
            charData = await this.serverTool("dict/character", char)
        } else {
            for (var i = 0; i < char.length; i++) {
                var c = char[i]
                var d = await this.serverTool("dict/character", c)
                decomp.push(d)
            }
        }
        await this.setState({
            examples: examples,
            decomp: decomp,
            charData: charData
        })

    };

    serverTool(attr, phrase) {
        return new Promise(function (resolve, reject) {
            // Do async job
            instance.get('/api/' + attr + '/' + phrase)
                .then(response => {
                    resolve(response.data)
                }).catch(err => reject(err))
        })
    }

    listDefinitions() {
        var defs = this.props.data.definition.split("/")
        // console.log(defs)
        var lists = []
        for (var i in defs) {
            if (defs[i] != "") {
                lists.push(
                    <span><span style={{ fontWeight: "bold" }}>{parseInt(i) + 1}.</span> {defs[i]} </span>
                )
            }
        }
        return lists
    }

    delete() {
        this.props.delete(this.props.id)
    }

    getId() {
        // console.log("id", this.props.id)
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <Divider />
                <div className={classes.card} onClick={this.getId}>
                    <div >
                        {this.props.close ?
                            <IconButton className={classes.button} aria-label="Delete" onClick={this.delete.bind(this)}>
                                <ClearIcon />
                            </IconButton> : ""
                        }
                        <div className="container">
                            <div class="row justify-content-md-center align-middle">
                                <div className="col" style={{ width: "100%" }}>
                                    <Typography variant="headline" gutterBottom>
                                        <ResultChar handleClick={this.props.charClick} clickable={this.props.clickable} data={this.props.data} />
                                    </Typography>
                                    <Typography style={{ maxWidth: 500, "textAlign": "center" }}>
                                        {/* {this.props.data.definition} */}
                                        {this.listDefinitions()}
                                    </Typography>
                                </div>
                                <div className="align-middle" style={{ height: "100%" }}>
                                    <div className="d-flex flex-column justify-content-center">
                                        {/* {this.props.close ?
                                            <IconButton className={classes.button} aria-label="Delete" onClick={this.delete.bind(this)}>
                                                <ClearIcon />
                                            </IconButton> : ""
                                        } */}
                                        <IconButton className="col-md-auto"
                                            className={classnames(classes.expand, {
                                                [classes.expandOpen]: this.state.expanded,
                                            })}
                                            onClick={this.handleExpandClick}
                                            aria-expanded={this.state.expanded}
                                            aria-label="Show more"
                                        >
                                            <ExpandMoreIcon />
                                        </IconButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <CardActions className={classes.actions} disableActionSpacing>
                        <IconButton aria-label="Add to favorites">
                            <FavoriteIcon />
                        </IconButton>
                        <IconButton aria-label="Share">
                            <ShareIcon />
                        </IconButton>
                        <IconButton
                            className={classnames(classes.expand, {
                                [classes.expandOpen]: this.state.expanded,
                            })}
                            onClick={this.handleExpandClick}
                            aria-expanded={this.state.expanded}
                            aria-label="Show more"
                        >
                            <ExpandMoreIcon />
                        </IconButton>
                    </CardActions> */}
                    <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                        <CardContent>
                            <Typography>
                                {(this.state.examples == null && this.state.decomp == null) ?
                                    <CircularProgress className={classes.progress} />
                                    :
                                    <Details id={this.props.id} key={this.key} searchExample={this.props.handleExample} data={this.props.data}
                                        examples={this.state.examples} decomp={this.state.decomp} charData={this.state.charData} exampleClick={this.props.handleExample} />

                                }
                            </Typography>
                        </CardContent>
                    </Collapse>

                </div>
                <Divider />
            </div>
        );
    }
}

ResultCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ResultCard);