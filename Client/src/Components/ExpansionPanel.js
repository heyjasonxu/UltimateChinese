import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AnimeCharacter from "./AnimeCharacter";
import pinyinizer2 from '../pinyinizer-2';
import axios from 'axios';
import HoverClick from "./HoverClick"
const instance = axios.create({ baseURL: 'http://jasonxu.me:5000' })

const styles = theme => ({
    root: {
        width: '500px',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
});



class SimpleExpansionPanel extends React.Component {
    constructor() {
        super()
        this.state = {
            etym: "",
            hover: false,
        }
        this.exampleClick = this.exampleClick.bind(this)
    }

    componentDidMount() {
        // this.getEtym()
    }



    getComponents() {
        var list = []
        if (this.props.decomp.length == 1) {
            var d = this.props.decomp[0]
            list.push(
                <div>
                    <span>Decompose Once</span>  -  <span>{d.components1}</span>
                </div>
            )
            list.push(
                <div>
                    <span>Decompose Radical</span>  -  <span>{d.components2}</span>
                </div>
            )
        } else {
            for (var i in this.props.decomp) {
                var char = this.props.decomp[i]
                list.push(
                    <li>
                        <span style={{ fontSize: 16, fontWeight: "bold", color: "blue" }}><HoverClick word={char[0].character} pinyin={char[0].pinyin[0]} exampleClick={this.exampleClick.bind(this, char[0].character, char[0].pinyin[0])} /></span> ({char[0].pinyin[0]}) - {char[0].definition}
                    </li>
                )
            }
        }
        return list
    }

    getEtym() {
        var list = []
        console.log("char", this.props.charData)
        if (this.props.charData && this.props.decomp.length == 1) {
            if (this.props.charData.length > 0) {
                if (this.props.charData[0].etymology) {
                    var data = this.props.charData[0]
                    if (data.etymology["type"] == "ideographic") {
                        list.push(
                            <div>
                                <span>Ideographic</span> - <span>{this.props.charData[0].etymology.hint}</span>
                            </div>)
                    } else if (data.etymology["type"] == "pictophonetic") {
                        // var s = await this.serverTool("dict/character", data.etymology.semantic)
                        // var p = await this.serverTool("dict/character", data.etymology.phonetic)
                        list.push(
                            <div>
                                <div>Semantic - <span style={{ fontFamily: "KaiTi", fontSize: 22, }}>{data.etymology.semantic}</span></div>
                                <div>Phonetic - <span style={{ fontFamily: "KaiTi", fontSize: 22, }}>{data.etymology.phonetic}</span></div>
                            </div>)
                    }
                }
            }
        }
        // await this.setState({
        //     e: list
        // })
        // console.log("e", this.state.e)

        return list
    }

    serverTool(attr, phrase) {
        return new Promise(function (resolve, reject) {
            // Do async job
            instance.get('/api/' + attr + '/' + phrase)
                .then(response => {
                    resolve(response.data)
                }).catch(err => reject(err))
        })
    }

    exampleClick(word, pinyin) {
        if (this.props.exampleClick) {
            this.props.exampleClick(word, pinyin)
        }

    }

    getExamples() {
        // console.log("examples", this.props.examples)
        var list = []
        var count = 0;
        var examples = this.props.examples
        var style = {
            hover: {
                fontWeight: "bold",
                fontSize: 16,
                textDecoration: "underline",

            },
            notHover: {
                fontWeight: "bold",
                fontSize: 16
            }
        }
        for (var i in examples) {
            for (var j in examples[i]) {
                var pinyin = examples[i][j]["pinyin"].replace("u:", "v").toLowerCase()
                var example = examples[i][j][this.props.data.charSet.toLowerCase()]
                if ((example !== this.props.data.characters) && pinyin.includes(this.props.data.numPinyin.toLowerCase())) { //no repeat in examples
                    count++
                    // list += count + examples[i][j][this.props.charSet] + " "
                    var word = examples[i][j][this.props.data.charSet.toLowerCase()]
                    var def = examples[i][j].definition
                    var pinyin = pinyinizer2.pinyinize(examples[i][j].pinyin)
                    list.push(
                        <li><span style={{ fontSize: 16, fontWeight: "bold", color: "blue" }}><HoverClick word={word} pinyin={pinyin} exampleClick={this.exampleClick.bind(this, word, pinyin)} /></span> ({pinyin}) - {def}</li>

                    )
                    if (count == 12) {
                        break;
                    }
                }
            }
            if (count == 12) {
                break;
            }
        }
        return list
    }

    getStroke() {
        var list = [];
        var word = this.props.data.characters
        for (var i in word) {
            var char = word[i]
            list.push(<AnimeCharacter character={char} altChar={this.props.data.altChar[i]} id={this.props.id + "" + i} key={this.key + "" + i} />)
            // console.log("character-id", this.props.id + "" + i)
            // console.log("character-key", this.key + "" + i)
        }
        return list

    }

    render() {
        const { classes } = this.props;
        var etymology = this.getEtym()
        var examples = this.getExamples()

        return (
            <div>
                {false ? ""
                    :
                    <div className={classes.root}>
                        <ExpansionPanel defaultExpanded>
                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                <Typography className={classes.heading}>Stroke Order</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                <div className="d-flex mx-auto">
                                    {this.getStroke()}
                                </div>
                            </ExpansionPanelDetails>
                        </ExpansionPanel>

                        <ExpansionPanel>
                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                <Typography className={classes.heading}>Components</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                <Typography>
                                    <div style={{ textAlign: "left" }}>
                                        <ul>
                                            {this.getComponents()}
                                        </ul>
                                    </div>

                                </Typography>
                            </ExpansionPanelDetails>
                        </ExpansionPanel>

                        {examples.length > 0 ?
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    <Typography className={classes.heading}>Examples</Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <Typography>
                                        <div style={{ textAlign: "left" }}>
                                            <ul>
                                                {examples}
                                            </ul>
                                        </div>
                                    </Typography>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                            :
                            ""
                        }


                        {etymology.length > 0 ?
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    <Typography className={classes.heading}>Etymology</Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <Typography>
                                        {etymology}
                                    </Typography>
                                </ExpansionPanelDetails>
                            </ExpansionPanel> :
                            ""
                        }

                        <ExpansionPanel disabled>
                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                <Typography className={classes.heading}>Sentences</Typography>
                            </ExpansionPanelSummary>
                        </ExpansionPanel>
                    </div>
                }
            </div>
        );
    }
}

SimpleExpansionPanel.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleExpansionPanel);