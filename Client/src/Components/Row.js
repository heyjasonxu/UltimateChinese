import React, { Component } from 'react';
import Card from "./Card"
import axios from 'axios'
const instance = axios.create({ baseURL: 'http://jasonxu.me:5000' })



export default class Row extends Component {

    constructor() {
        super()
        this.state = {
            viewExampleChar: false,
            exampleData: "",
            cards: [],
            sideCards: [],
            count: 0,
        }
        // this.handleExampleClick = this.handleExampleClick.bind(this)
    }

    componentDidMount() {
        var cards = [];
        var count = this.state.count
        cards.push(<Card characters={this.props.characters} altChar={this.props.altChar} id={this.props.id + "" + count} key={this.key + "" + count}
            def={this.props.def} charSet={this.props.charSet} pinyin={this.props.pinyin} examples={this.props.examples}
            clickable={this.props.data.characters.length === 1 ? false : true}
            numPinyin={this.props.numPinyin} handleExample={this.handleExampleClick} closeExample={this} charClick={this.handleExampleClick}
            data={this.props.data} />)
        count++
        this.setState({
            cards: cards,
            count: count
        })
    }

    handleExampleClick = async function (data, pinyin, e) {
        console.log("3")
        console.log("data", data)
        console.log("pinyin", pinyin)
        // e.preventDefault()
        await this.setState({
            viewExampleChar: false
        });
        var exampleData = await this.queryServer(this.props.data.charSet.toLowerCase(), data)
        // console.log("data", exampleData)
        var cards = this.state.cards
        var count = this.state.count
        var sideCards = this.state.sideCards


        if (exampleData.length === 0) {
            alert("No Results")
        } else {
            for (var i in exampleData) {
                // console.log("pinyin", pinyin)
                // console.log("2", exampleData[i].PrettyPinyin)
                if (pinyin.toLowerCase() === exampleData[i].PrettyPinyin.toLowerCase()) {
                    var cardData = {
                        characters: exampleData[i][this.props.data.charSet],
                        charSet: this.props.data.charSet,
                        altChar: exampleData[i][this.props.data.altSet],
                        pinyin: exampleData[i].PrettyPinyin,
                        numPinyin: exampleData[i].NumPinyin,
                        definition: exampleData[i].Definition,
                        examples: "",
                        font: this.props.data.font
                    }
                    sideCards.push(<Card characters={exampleData[i][this.props.charSet]} altChar={exampleData[i][this.props.altSet]} id={this.props.id + "" + count}
                        key={this.props.key} def={exampleData[i].Definition} charSet={this.props.charSet} clickable={false}
                        pinyin={exampleData[i].PrettyPinyin} examples={""} numPinyin={exampleData[i].numPinyin} close={true} delete={this.delete.bind(this)}
                        data={cardData} />)
                    count++
                }

            }

            await this.setState({
                exampleData: exampleData,
                viewExampleChar: true,
                cards: cards,
                sideCards: sideCards,
                count: count
            });
        }

        // console.log(this.state.sideCards)
    }.bind(this);

    // handleCharacterClick = async function (data) {
    //     // console.log("data", data)
    //     // e.preventDefault()
    //     await this.setState({
    //         viewExampleChar: false
    //     });
    //     var exampleData = await this.queryServer(this.props.charSet.toLowerCase(), data)

    //     var count = this.state.count
    //     var sideCards = []
    //     for (var i in exampleData) {
    //         sideCards.push(<Card id={this.props.id + "" + count} key={this.key + "" + count} clickable={false}
    //             close={true} delete={this.delete.bind(this)} data={this.props.data}
    //             characters={exampleData[0][this.props.charSet]} altChar={exampleData[0][this.props.altSet]}
    //             def={exampleData[0].Definition} charSet={this.props.charSet} pinyin={exampleData[0].PrettyPinyin} examples={""}
    //             numPinyin={exampleData[0].numPinyin} />)
    //         count++
    //         // console.log("card-id", this.props.id + "" + i)
    //         // console.log("card-key", this.key + "" + i)
    //     }

    //     await this.setState({
    //         exampleData: exampleData,
    //         viewExampleChar: true,
    //         sideCards: sideCards,

    //         count: count
    //     });
    // }

    async delete(id) {
        // console.log(id)
        // console.log("row", this.state.cards)
        var sideCards = this.state.sideCards
        for (var i in sideCards) {
            if (sideCards[i].props.id === id) {
                // console.log("check", cards[i].props.id)
                sideCards.splice(i, 1)
                break;
            }
            // console.log(this.state.card[i].props)
        }
        this.setState({
            sideCards: sideCards
        })
    }

    queryServer(attr, phrase) {
        return new Promise(function (resolve, reject) {
            // Do async job
            instance.get('/api/dict/zh/' + attr + '/' + phrase)
                .then(response => {
                    resolve(response.data)
                }).catch(err => reject(err))
        })
    }

    getCards() {
        return this.state.cards;
    }
    getSideCards() {
        // console.log("side", this.state.sideCards)
        return this.state.sideCards;
    }

    render() {
        return (
            <div class="d-flex justify-content-start">
                <div>
                    {this.getCards()}
                </div>
                <div>
                    {this.getSideCards()}
                </div>
            </div>
        );
    }
}