import { createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk";
import rootReducer from "./reducers"

const initialState = {
    // charSet: {
    //     main: "Simplified",
    //     sec: "Traditional"
    // },
    // font: "KaiTi"
};

const middleware = [thunk];

const store = createStore(
    rootReducer,
    initialState,
    // windows.devToolsExtension && window.devToolsExtension()
    applyMiddleware(...middleware)
);

export default store;