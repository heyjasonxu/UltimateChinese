var fs = require("fs");
var pinyinizer = require("../pinyinizer-2")

function separateByLine(data) {
    return data.split("\n")
}

function removePinyinSpace(string) {
    var newString = ""
    for (var i in string) {
        if (string[i] !== " ") {
            newString += string[i]
        }
    }
    return newString
}

function removeNumPinyin(string) {
    var num = "12345 ";
    var newString = "";
    for (var i in string) {
        if (!num.includes(string[i])) {
            newString += string[i]
        }
    }
    return newString
}

function vPinyin(string) {
    var newString = "";
    for (var i in string) {
        if (string[i] == "ü") {
            newString += 'v'
        } else {
            newString += string[i]
        }
    }
    return newString
}


class cedictToCSV {
    constructor(file) {
        this.file = file;
        this.data = "";
        this.lines = [];

    }

    getCSV() {
        console.log("test")
    }



    parseFile() {
        fs.readFile(this.file, "utf8", function (err, data) {
            var lines = separateByLine(data)
            var csvFinal = []
            csvFinal.push(["Traditional", "Simplified", "PrettyPinyin", "NumPinyin", "PlainPinyin", "Definition"])
            for (var i = 30; i < lines.length; i++) {
                var line = lines[i]
                var lineArray = []
                var temp = 0;
                for (var j in line) {
                    if (line[j] == '[') {
                        var chars = line.substring(0, j - 1)
                        var charSep = chars.split(" ")
                        for (var k in charSep) {
                            lineArray.push(charSep[k])
                        }
                        temp = j;
                    }
                    if (line[j] == "/") {
                        var pinyin = line.substring(temp, j - 1)
                        pinyin = pinyin.substring(1, pinyin.length - 1)
                        var PrettyPinyin = pinyinizer.pinyinize(pinyin)
                        lineArray.push(PrettyPinyin)
                        var v_Pinyin = vPinyin(pinyin)
                        lineArray.push(v_Pinyin)
                        // lineArray.push(removePinyinSpace(pinyin))
                        lineArray.push(removeNumPinyin(v_Pinyin))
                        var def = line.substring(j, line.length - 1)
                        lineArray.push(def)
                        break;
                    }
                }
                csvFinal.push(lineArray);
            }
            let csvContent = "data:text/csv;charset=utf-8,";
            csvFinal.forEach(function (rowArray) {
                let row = rowArray.join(";");
                csvContent += row + "\r\n";
            });
            fs.writeFile('formList.csv', csvContent, 'utf8', function (err) {
                if (err) {
                    console.log('Some error occured - file either not saved or corrupted file saved.');
                } else {
                    console.log('It\'s saved!');
                }
            });
        })
    }
}

// var t = new cedictToCSV("../data/cedict_1_0_ts_utf-8_mdbg.txt");
// t.parseFile();
// var fs = require('fs');
// var rstream = fs.createReadStream('./ceDictFormated.csv');
// var wstream = fs.createWriteStream('./mysql.sql');

// let csvToJson = require('convert-csv-to-json');

// let fileInputName = './formList.csv'; 
// let fileOutputName = 'new.json';

// csvToJson.generateJsonFileFromCsv(fileInputName,fileOutputName);