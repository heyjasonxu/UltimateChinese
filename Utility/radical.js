var radicals = require("./radicals.json")
var fs = require("fs");

function addSimplified(input) {
    var newList = []
    for(var i in input) {
        var obj = input[i]
        obj["Radical (Simplified)"] = obj["Radical (Traditional)"]
        var ordered = JSON.parse(JSON.stringify( obj, ["No.", "Radical (Traditional)","Radical (Simplified)","Stroke count", "Meaning", "Pīnyīn", "Sino-Vietnamese(Hán-Việt)", "Hiragana-Romaji", "Hangeul-Romaja", "Frequency", "Simplified", "Examples", "TradExamples"] , 2));
        newList.push(ordered)
    }
    newList = JSON.stringify(newList, null, 2)
    fs.writeFile("newRad.json", newList, 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
    
    
    return newList
}

console.log(addSimplified(radicals))