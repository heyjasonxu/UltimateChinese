const scrapeIt = require("scrape-it")
const url = "https://en.wiktionary.org";

// get wiki mandarin frequency list, save to file
// Promise interface
async function wikiLinks() {
    scrapeIt(url + "/wiki/Appendix:Mandarin_Frequency_lists", {
        articles: {
            listItem: "b a",
            data: {
                link: {
                    attr: "href"
                }
            }
        }

    }).then(async function ({ data, response }) {
        console.log(`Status Code: ${response.statusCode}`)
        parseLinks(data.articles)
        // console.log(data)
        // console.log(data.articles)
        // for(var i in data.articles) {
        //     console.log(data.articles[i])
        // }
        // scrapeIt("https://en.wiktionary.org/wiki/Appendix:Mandarin_Frequency_lists", {
        //     articles: {
        //         listItem: "b a",
        //         data: {
        //             link: {
        //                 attr: "href"
        //             }
        //         }
        //     }

        // }).then(async function ({ data, response }) {
        //     console.log(`Status Code: ${response.statusCode}`)
        //     // console.log(data)
        //     this.list = await data
        // })
    })
}

async function parseLinks(data) {
    console.log(url + data[0])
    console.log(data)
    const list = []
    for (var i in data) {
        await scrapeIt(url + data[i].link, {
            list: {
                listItem: "li",
                data: {
                    word: {
                        selector: "span"
                    }

                }
            }

        }).then(async function ({ data, response }) {
            console.log(`Status Code: ${response.statusCode}`)
            // console.log(data.list)
            // const list = []
            for (var j in data.list) {
                var word = data.list[j].word
                var halfLength = word.length / 2
                var split = word.substring(0, halfLength) + "," + word.substring(halfLength)
                list.push(split)
            }
            // console.log(list)
        })
    }
    // console.log(list)
    exportToFile(list)
    // writeTextFile("./text.txt", list.toString())
    // saveToJSON(list)


}

function exportToFile(file) {
    var fs = require('fs');
    var fileName = 'frqList.txt';
    fs.writeFileSync(fileName, file.join('\n'));
}

function writeTextFile(filepath, output) {
    var file = new File("./text.txt");
    var txtFile = new File(filepath);
    txtFile.open("w"); //
    txtFile.writeln(output);
    txtFile.close();
}

function saveToJSON(file) {
    let fs = require('fs');

    fs.readFile('file.json', 'utf8', function (err, data) {
        if (err) {
            console.log(err)
        } else {
            const file = JSON.parse(data);
            file.events.push({ "id": title1, "year": 2018, "month": 1, "day": 3 });
            file.events.push({ "id": title2, "year": 2018, "month": 2, "day": 4 });

            const json = JSON.stringify(file);

            fs.writeFile('/path/to/local/file.json', json, 'utf8', function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Everything went OK!
                }
            });
        }

    });
}

wikiLinks()
