var dict = require("./dictData.json")
var ranked = require("./test.json")
var onlyRanked = require("./moved.json")
var fs = require("fs");
//var sorted = require("./sortedRank.json");
var final = require("./radicals1.json")
var sorted = require("./table-2.json")
// var rank = require("./freqList.txt")
// console.log(dict[0][0])
// console.log(rank[0])


function separateByLine(data) {
    return data.split("\n")
}

// function rankJSON(list) {
//     var newDict = dict
//     for(var i in newDict) {
//         console.log(i)
//         for(var j in list) {
//             if(newDict[i].Simplified == list[j]) {
//                 newDict[i].Rank = j
//                 // console.log(dict[i])
//             }
//         }
//         if(newDict[i].rank == null) {
//             newDict[i].Rank = "N/A"
//         }
//     }
//     return newDict
// }

function rankJSON(list) {
    var newDict = dict
    for (var i in newDict) {
        console.log(i)
        for (var j in list) {
            if (newDict[i].Simplified == list[j]) {
                newDict[i].Rank = j
                // arr.splice(, 0, "Lene");
                // console.log(dict[i])
            }
        }
        if (newDict[i].Rank == null) {
            newDict[i].Rank = "N/A"
        }
    }
    return newDict
}

function sortDict(list) {
    var newList = list
    for (var i in newList) {
        console.log(i)
        if (newList[i].Rank !== "N/A") {
            var rank = parseInt(newList[i].Rank);
            var obj = newList[i]
            var ok = false
            while (!ok) {
                if (newList[rank].Simplified != newList[i].Simplified) {
                    ok = true;
                    newList.splice(i, 1);
                    newList.splice(rank, 0, obj);

                    if (rank > i) {
                        i--
                    }
                } else {
                    rank++;
                }
            }


            // newList.splice(rank, 0, obj);
            // if (rank > i) {
            //     newList.splice(i, 1);
            //     i--
            // } else {
            //     newList.splice(i + 1, 1);
            //     i++
            // }
        }
    }
    newList = JSON.stringify(newList, null, 2)
    fs.writeFile("sort.json", newList, 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
}

function moveRanked(list) {
    var nList = list;
    var p = [];
    var count = 0;
    for (var i in nList) {
        console.log(i)
        if (nList[i].Rank !== "N/A") {
            var obj = nList[i]
            // nList.splice(count, 0, obj);
            // nList.splice(i, 1);
            count++;
            p.push(obj)
        }
    }
    // for(var i in nList) {
    //     console.log(i)
    //     if(nList[i].Rank == "N/A") {
    //         var obj = nList[i]
    //         // nList.splice(count, 0, obj);
    //         // nList.splice(i, 1);
    //         count++;
    //         p.push(obj)
    //     }
    // }
    fs.writeFile("moved.json", JSON.stringify(p, null, 2), 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
}

function sortRanked(list) {
    var unsorted = list;
    var sorted = [];
    for (var i = 1; i <= 25000; i++) {
        console.log(i)
        for (var j in unsorted) {
            if (parseInt(unsorted[j].Rank) == i) {
                sorted.push(unsorted[j])
            }
        }
    }
    sorted = JSON.stringify(sorted, null, 2)
    fs.writeFile("sortedRank.json", sorted, 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
}

function saveJSON() {
    fs.readFile("./giga-zh.txt", "utf8", function (err, data) {
        var lines = separateByLine(data)
        var list = []
        for (var i in lines) {
            var stop = lines[i].indexOf("[")
            list.push(lines[i].substring(0, stop))
        }
        console.log(list)
        var newJSON = JSON.stringify(rankJSON(list), null, 2)
        fs.writeFile("test.json", newJSON, 'utf8', function (err) {
            if (err) {
                console.log(err);
            }
        });
    })
}

function addUnranked(ranked, unranked) {
    var sorted = ranked
    for (var i in unranked) {
        if (unranked[i].Rank == "N/A") {
            sorted.push(unranked[i]);
        }
    }
    sorted = JSON.stringify(sorted, null, 2)
    fs.writeFile("final.json", sorted, 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
}

function lowerDef(list) {
    var newList = list
    for (var i in newList) {
        console.log(i)
        newList[i].LowDef = newList[i].Definition.toLowerCase();

    }
    newList = JSON.stringify(newList, null, 2)
    fs.writeFile("newfinal.json", newList, 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
}

function sortRad(list, sorted) {
    var newSorted = []
    for (var i in sorted) {
        // console.log(sorted[i])
        for(var j in list) {
            if(sorted[i]['Meaning'] == list[j]['Meaning']) {
                console.log(sorted[i]['Radical (variants)'])
                // var obj = list[j]
                list[j]["Frequency Rank"] = parseInt(i) + 1

                newSorted.push(list[j]);
                break;
            }
        }
    }
    newSorted = JSON.stringify(newSorted, null, 2)
    fs.writeFile("sortedRad.json", newSorted, 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
}

function check(list) {
    var j = []
    for(var i in list) {
        j.push(list[i])
    }
    j = JSON.stringify(j, null, 2)
    fs.writeFile("j.json", j, 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
}

// saveJSON()
// sortDict(ranked)
// moveRanked(ranked)
// sortRanked(onlyRanked)
// addUnranked(sorted, ranked)
// lowerDef(final)
sortRad(final, sorted)
// check(final)



